import { DynamoDBClient } from "@aws-sdk/client-dynamodb";
import { PutCommand, GetCommand, DynamoDBDocumentClient } from "@aws-sdk/lib-dynamodb";

const client = new DynamoDBClient({});
const docClient = DynamoDBDocumentClient.from(client);

const headers = {
  'Access-Control-Allow-Origin': '*',
  'Access-Control-Allow-Credentials': true,
};

export const getScoreData = async (event) => {
  const userId = event.queryStringParameters.user;

  const command = new GetCommand({
    ConsistentRead: true,
    AttributesToGet: [
      "scoreJson",
    ],
    TableName: 'score-data-2',
    Key: {
      "userId": userId
    },
  });

  try {
    const resp = await docClient.send(command)
    const data = JSON.parse(resp.Item.scoreJson);

    return {
      statusCode: 200,
      headers: headers,
      body: JSON.stringify({
        status: "success",
        data: data,
      }, null, 2),
    };

  } catch (err) {
    return {
      statusCode: 400,
      headers: headers,
      body: JSON.stringify({status: "error", "Err": err}),
    };
  }
};

export const setScoreData = async (event) => {
  const data = JSON.parse(event.body).data;
  const dataString = data.score;
  const userId = data.username;

  const command = new PutCommand({
    TableName: 'score-data-2',
    Item: {
      "userId": userId,
      "scoreJson": JSON.stringify(dataString),
      "lastModified": new Date().toJSON(),
    },
  });

  try {
    const response = await docClient.send(command);

    return {
      statusCode: 200,
      headers: headers,
      body: JSON.stringify({
        status: "success",
        data: dataString,
      }, null, 2),
    };

  } catch (err) {
    return {
      headers: headers,
      statusCode: 400,
      body: JSON.stringify({status: "error", "Err": err}),
    };
  }
};

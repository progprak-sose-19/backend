# Installation

1. install serverless with `npm install -g serverless`
2. set the aws credentials
3. run `sls deploy`
4. find and copy the url of the deployed lambda (you can also edit it if you set up a custom domain with cloudfront)
5. set the url in the frontend project in `src/Storage/BackendStorage.ts:4`
6. run `npm run build` at the frontend
